//var app=require("../add.js");
//describe("Addition",function(){
//it("The function should add 2 numbers",function() {
//var value=app.AddNumber(5,6);
//expect(value).toBe(11);
//});
//});


var request = require("request");
var helloWorld = require("../app1.js")
var base_url = "http://localhost:3000/"

describe("Hello World Server", function() {
  describe("GET /", function() {
    it("returns status code 200", function(done) {
      request.get(base_url, function(error, response, body) {
        expect(response.statusCode).toBe(200);
        done();
      });
    });

    it("returns Hello World", function(done) {
      request.get(base_url, function(error, response, body) {
        expect(body).toBe("Hello World");
        helloWorld.closeServer();
        done();
      });
    });
  });
});

