var express = require('express');
var router = express.Router();

var busBookingID =0;

router.get('/busDetails', function(req, res) {
    req.busDetails.find({}, function(err, bus) {
        if (err)
            res.send(err);
        else {
            res.json(bus);
        }
    });
});

router.post('/busBooking', function(req, res){
    busBookingID = Math.floor(Math.random() * 205);
    var bookingDetails = new req.bookedBusDetails({
    BookingId: busBookingID,
    UserName: req.body.UserName,
    BusRouteNumber: req.body.BusRouteNumber,
    BusDescription: req.body.BusDescription,
    FromCity: req.body.FromCity,
    ToCity: req.body.ToCity,
    TotalSeats : req.body.TotalSeats,
    DateBooked : req.body.DateBooked,
    BookedSeats : req.body.BookedSeats,
    AvailableSeats : req.body.AvailableSeats
    });
  
    bookingDetails.save(function(err) {
      if (err){
      res.send(err);
      }
      else {      
         res.json(bookingDetails);
      }
    });
});

router.get('/bookingHistory/:username', function(req, res) {
    req.bookedBusDetails.find({UserName:req.params.username}, function(err, bus) {
        if (err)
            res.send(err);
        else {            
            res.json(bus);
        }
    });
});

router.get('/busBooking', function(req, res) {
    req.bookedBusDetails.find({}, function(err, bus) {
        if (err)
            res.send(err);
        else {            
            res.json(bus);
        }
    });
});

router.put('/busDetails', function(req, res) {
    req.busDetails.update ({BusRouteNumber: req.body.BusRouteNumber}, {$set: {
      TotalSeats : req.body.TotalSeats
   }
  },function(err){
    if (err) 
    res.send(err);
    else
    res.json("details updated successfully ");
  })
  });

module.exports = router;