var express = require('express');
var router = express.Router();
var passport = require('passport');    
        

router.get('/redirect', isLoggedIn, function(req, res) {
   // console.log("inside redirect",req.user ,"hi");
   
});
    
        // route for twitter authentication and login
        router.get('/auth/twitter', passport.authenticate('twitter'));
    
        // handle the callback after twitter has authenticated the user
        router.get('/auth/twitter/callback',
            passport.authenticate('twitter', {
                successRedirect : '/redirect',
                failureRedirect : '/redirect'
            }));
    
    
    // route middleware to make sure a user is logged in
    function isLoggedIn(req, res, next) {
    
        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();
    
        // if they aren't redirect them to the home page
        res.redirect('/');
    }

    module.exports = router;
    